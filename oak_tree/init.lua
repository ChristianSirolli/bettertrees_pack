bettertrees_api.register_tree('oak_tree:oak', {
    description = 'Oak Tree',
    fruit_same_as_seed = true, --acorns are planted directly
    trunk = {
        tiles = {"oak_tree_top.png", "oak_tree_top.png", "oak_tree.png"},
    },
    leaves = {
        texture = 'oak_tree_leaves.png',
    },
    planks = {
        texture = 'oak_tree_wood.png'
    },
    fruit = {
        name = 'oak_tree:acorn',
        description = 'Acorn',
        edible = {
            raw = false,
            cooked = true,
        },
    },
    sticks = {
        texture = 'oak_tree_stick.png',
    },
    sapling = {
        texture = 'oak_tree_sapling.png'
    }
})
