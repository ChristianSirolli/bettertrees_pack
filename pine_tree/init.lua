bettertrees_api.register_tree('pine_tree:pine', {
    description = 'Pine Tree',
    trunk = {
        tiles = {"pine_tree_top.png", "pine_tree_top.png", "pine_tree.png"},
    },
    leaves = {
        name = 'pine_tree:pine_needles',
        description = 'Pine Needles',
        texture = 'pine_tree_needles.png',
    },
    planks = {
        texture = 'pine_tree_wood.png'
    },
    fruit = {
        name = 'pine_tree:pine_cone',
        description = 'Pine Cone',
        edible = {
            raw = false,
            cooked = false,
        },
    },
    seeds = {
        description = 'Pine Nuts',
        name = 'pine_tree:pine_nuts',
        edible = {
            raw = true,
            cooked = true,
        }
    },
    sticks = {
        texture = 'pine_tree_stick.png',
    },
    sapling = {
        texture = 'pine_tree_sapling.png'
   }
})
